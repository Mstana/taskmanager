//
//  Task.swift
//  
//
//  Created by Marek Stana on 15/10/15.
//
//

import Foundation
import CoreData

class Task: NSManagedObject {

    @NSManaged var completed: NSNumber
    @NSManaged var name: String
    @NSManaged var id: String
    @NSManaged var synced: String
    @NSManaged var userId: NSNumber

}
