//
//  DataManager.swift
//  
//
//  Created by Marek Stana on 15/10/15.


import UIKit
import CoreData


public class DataManager: NSObject {
    
    public class func getContext() -> NSManagedObjectContext {
        
        return CoreDataProxy.sharedInstance.managedObjectContext!
    }
    

    public class func deleteManagedObject(object:NSManagedObject) {
        getContext().deleteObject(object)
        saveManagedContext()
    }
    
    public class func saveManagedContext() {
        var error : NSError? = nil
        if !getContext().save(&error) {
            NSLog("Unresolved error saving context \(error), \(error!.userInfo)")
            abort()
        }
    }
    
}