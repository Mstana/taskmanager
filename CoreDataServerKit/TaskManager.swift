//
//  TaskManager.swift
//  
//
//  Created by Marek Stana on 15/10/15.
//
//


import CoreData
import SwiftyJSON

public class TaskManager: NSObject {
    
    
    public static let entityName = "Task"
    static let sharedInstance = TaskManager()
    static let model:DataModel = DataModel()
    static let context = DataManager.getContext()
    
    public class func createTaskWithCD(token:String, name: NSString, completed:Bool, completionHandler: (responseObject: JSON?, error: NSError?) -> ()) {
        println("1")
        
        let newTask: Task = NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: context) as! Task
        
        self.model.createTask(token, title: name as String, completed: completed){ (responseObject, error) -> () in
            
            
            if((error) != nil){
                
                newTask.name = String(name)
                newTask.completed = completed
                newTask.synced = "create"
                NSLog("note saving with text \" "  + newTask.name + "\" and id: nil")
            } else {
                
                //everything ok
                newTask.name = String(name)
                newTask.completed = completed
                newTask.synced = "ok"
                newTask.taskId = responseObject!["id"].stringValue
                NSLog("note saving with text \" "  + newTask.name + "\" and id:" + newTask.taskId!)
            }
            DataManager.saveManagedContext()
            completionHandler(responseObject: nil, error: error)
        }
    }
    
    public class func createTaskLocal(id: String, name: String, completed:Bool)  {
        println("2")
        
        let newTask: Task = NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: context) as! Task
    
            //everything ok
        newTask.name = String(name)
        newTask.completed = completed
        newTask.synced = "ok"
        newTask.taskId = id as String
        NSLog("note saving with text \" "  + newTask.name + "\" and id:" + newTask.taskId!)
        
        DataManager.saveManagedContext()
        
    }
    
    
    
    public class func updateTaskWithCD(token:String, name:String, completed: Bool, task:Task, completionHandler: () -> ())  {
        
        if let id = task.taskId {
            
            self.model.updateTask(token, id: task.taskId, title: name as String, completed: completed as Bool){ (responseObject, error) -> () in
                
                var fetchRequest = NSFetchRequest(entityName: self.entityName)
                fetchRequest.predicate = NSPredicate(format: "taskId = %@", task.taskId!)
                
                if let fetchResults = self.context.executeFetchRequest(fetchRequest, error: nil) as? [NSManagedObject] {
                    
                    if fetchResults.count != 0 {
                        var managedObject = fetchResults[0]
                        managedObject.setValue(completed, forKey: "completed")
                        managedObject.setValue(name, forKey: "name")
                        
                        
                        if((error) != nil){
                            
                            managedObject.setValue("update", forKey: "synced")
                        } else {
                            
                            //everything ok
                            managedObject.setValue("ok", forKey: "synced")
                        }
                        println(managedObject)
                        DataManager.saveManagedContext()
                        
                    }
                    
                }
                completionHandler()
            }

        } else {
            task.name = name
            task.completed = completed
                
            DataManager.saveManagedContext()
            completionHandler()
        }
    }
    public class func deleteTaskLocal(task:Task) {
        
        DataManager.deleteManagedObject(task)
        DataManager.saveManagedContext()
    }
    
    
    public class func deleteTaskWithCD(token:String, task:Task, completionHandler: () -> ())  {
        if let id = task.getId() {
//            musim mazat zo servera
            self.model.deleteTask(token, id: id){ (responseObject, error) -> () in
                if((error) != nil){
//                    nemam pristup na server tak jej dam iba tag
                    task.synced = "delete"
                } else {
                    task.synced = "delete"
                    DataManager.deleteManagedObject(task)
                }
                DataManager.saveManagedContext()
                completionHandler()
            }
        } else {
            DataManager.deleteManagedObject(task)
            DataManager.saveManagedContext()
            completionHandler()
            // taska vytvorena ofline - nema id, zmazem iba lokalne
        }
        
        

    }
    
    
    public class func fetchTasks(token:String, completionHandler: (responseObject: [Task]) -> ()) {
   
        let fetchRequest = NSFetchRequest(entityName: entityName)
        var tasks:[Task] = self.context.executeFetchRequest(fetchRequest, error: nil) as! [Task]
        
        var tasksIdClient = [Int]()
        var tasksIdServer = [Int]()
        
        //Colect tasks in client
        for task in tasks {

            if let id = task.taskId {
                tasksIdClient.append(NSNumberFormatter().numberFromString(id)!.integerValue)
            }
        }
        
        
        //Colect tasks in client
        var json:JSON = JSON.nullJSON
        model.getAllTasks(token) { (responseObject, error) -> () in
            if((error) != nil){
                
                //OFFLINE
                var tempTasks:[Task] = []
                for task in tasks {
                    if task.synced != "delete" {
                        tempTasks.append(task)
                    }
                }
                completionHandler(responseObject: tempTasks)
                return

            } else {
                json = responseObject!
                println(json)
            }
            
            for item in json.arrayValue {
                tasksIdServer.append(item["id"].intValue as Int)
            }
            
            //create what is created on server
            for task_id in tasksIdServer {
                
                if !contains(tasksIdClient, task_id) {
                    
                
                    
                    for item in json.arrayValue {
                        if (item["id"].intValue as Int) == task_id {
                            

                           self.createTaskLocal(String(item["id"].intValue as Int), name: item["name"].stringValue as String, completed: item["completed"].boolValue as Bool)
                        }
                    }
                }
            }
            //delete what is deleted on server
            for task_id in tasksIdClient {
                
                if !contains(tasksIdServer, task_id) {
                    
                    for task in tasks {
                        
                        if let id = task.taskId {
                            
                            let taskIdInt = NSNumberFormatter().numberFromString(id)!.integerValue
                            
                            if (taskIdInt == task_id) {
                                NSLog("deleting note because was deleted on server. Id of note: " + task.taskId!)
                                self.deleteTaskLocal(task)
                            }
                        }
                    }
                }
            }
        
            //update
        
            tasks = self.context.executeFetchRequest(fetchRequest, error: nil) as! [Task]
        
            
            println(tasks)
            for task in tasks {
            
                switch (task.synced) {
                
                    case "ok":
                        break;
                    case "create":
                        self.model.createTask(token, title: task.name, completed: task.completed as! Bool){ (responseObject, error)-> () in
                        
                            if error == nil{
                                task.synced = "ok"
                                task.taskId = responseObject!["id"].stringValue
                            }
                        }
                        break;
                    case "update":
                        self.model.updateTask(token, id: task.taskId!, title: task.name, completed: task.completed as! Bool){ (responseObject, error) -> () in
                        
                            if error == nil{
                            
                                task.synced = "ok"
                            }
                        }
                        break;
                    case "delete":
                    
                        self.model.deleteTask(token, id: task.taskId!) { (responseObject, error) -> () in
                        
                            if error == nil{
                                DataManager.deleteManagedObject(task)
                                DataManager.saveManagedContext()
                            }
                        }
                        break;
                default:
                    break;
                }
            }
        
            var tempTasks:[Task] = []
            for task in tasks {
                if task.synced != "delete" {
                    tempTasks.append(task)
                }
            }
        

            completionHandler(responseObject: tempTasks)
            }
    
        }
}
