//
//  CoreDataServerKit.h
//  CoreDataServerKit
//
//  Created by Marek Stana on 15/10/15.
//  Copyright (c) 2015 Marek Stana. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CoreDataServerKit.
FOUNDATION_EXPORT double CoreDataServerKitVersionNumber;

//! Project version string for CoreDataServerKit.
FOUNDATION_EXPORT const unsigned char CoreDataServerKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoreDataServerKit/PublicHeader.h>


