# TestCheck

[![CI Status](http://img.shields.io/travis/3lvis/TestCheck.svg?style=flat)](https://travis-ci.org/3lvis/TestCheck)
[![Version](https://img.shields.io/cocoapods/v/TestCheck.svg?style=flat)](http://cocoadocs.org/docsets/TestCheck)
[![License](https://img.shields.io/cocoapods/l/TestCheck.svg?style=flat)](http://cocoadocs.org/docsets/TestCheck)
[![Platform](https://img.shields.io/cocoapods/p/TestCheck.svg?style=flat)](http://cocoadocs.org/docsets/TestCheck)

## Usage

```swift
import TestCheck

if Test.isRunning() {
    // Do something
}
```

## Installation

**TestCheck** is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TestCheck'
```

## License

**TestCheck** is available under the MIT license. See the LICENSE file for more info.

## Author

Elvis Nuñez, [@3lvis](https://twitter.com/3lvis)
