import Foundation

@objc public class Test: NSObject {
    public static func isRunning() -> Bool {
        let enviroment = NSProcessInfo.processInfo().environment
        let serviceName: AnyObject? = enviroment["XPC_SERVICE_NAME"]
        let injectBundle: AnyObject? = enviroment["XCInjectBundle"]
        var isRunning = (enviroment["TRAVIS"] != nil)

        if !isRunning {
            if let serviceName: AnyObject = serviceName {
                isRunning = (serviceName as! NSString).pathExtension == "xctest"
            }
        }

        if !isRunning {
            if let injectBundle: AnyObject = injectBundle {
                isRunning = (injectBundle as! NSString).pathExtension == "xctest"
            }
        }

        return isRunning
    }
}
