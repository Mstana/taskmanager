#import <UIKit/UIKit.h>

#import "NSManagedObject+ANDYObjectIDs.h"

FOUNDATION_EXPORT double NSManagedObject_ANDYObjectIDsVersionNumber;
FOUNDATION_EXPORT const unsigned char NSManagedObject_ANDYObjectIDsVersionString[];

