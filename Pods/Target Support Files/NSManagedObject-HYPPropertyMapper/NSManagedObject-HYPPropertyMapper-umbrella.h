#import <UIKit/UIKit.h>

#import "NSManagedObject+HYPPropertyMapper.h"

FOUNDATION_EXPORT double NSManagedObject_HYPPropertyMapperVersionNumber;
FOUNDATION_EXPORT const unsigned char NSManagedObject_HYPPropertyMapperVersionString[];

