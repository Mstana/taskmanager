//
//  LoginViewController.swift
//  
//
//  Created by Marek Stana on 12/10/15.
//
//

import UIKit
import SwiftyJSON
//import SwiftSpinner

class LoginViewController: UIViewController {
    
    let model:DataModel = DataModel()
    var json: JSON = JSON.nullJSON
    let defaults = NSUserDefaults.standardUserDefaults()
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    

    @IBOutlet weak var offlineUserButton: UIButton!
    @IBOutlet weak var button: UIButton!
    
    func register(sender:UIButton) {
        
        userName.text = ""
        password.text = ""
       
        defaults.removeObjectForKey("username")
        defaults.synchronize()
        
        button.setTitle("Register", forState: .Normal)
        
        var rightButtonBack:UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: "login:")
        self.navigationItem.rightBarButtonItem = rightButtonBack
    }
    
    func login(sender: UIButton) {
        
        
        userName.text = ""
        password.text = ""
        button.setTitle("Login", forState: .Normal)
       
        var rightButtonBack:UIBarButtonItem = UIBarButtonItem(title: "Sign up", style: UIBarButtonItemStyle.Plain, target: self, action: "register:")
        self.navigationItem.rightBarButtonItem = rightButtonBack
    }

    @IBAction func offlineUserChange(sender: AnyObject) {
        
        
        var alert = UIAlertController(title: "Offline User", message: "For offline user change you have to login with user in online mode first!", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        alert.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
            textField.placeholder = "User Name"
        })
        alert.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
            textField.placeholder = "Password"
        })
        
        var action = UIAlertAction(title: "Change", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            
            let textFieldName = alert.textFields![0] as! UITextField
            let textFieldPassword = alert.textFields![1] as! UITextField
            
            self.model.authorizeUser(textFieldName.text, password: textFieldPassword.text) { (resp, error) -> () in
                if error == nil {
                    
                    self.offlineUserButton.setTitle(textFieldPassword.text, forState: .Normal)
                    KeychainWrapper.setString(textFieldName.text, forKey: "username")
                    KeychainWrapper.setString(textFieldPassword.text, forKey: "password")
                }
            }
        }
        
        alert.addAction(action)
        
        self.presentViewController(alert, animated: true) { () -> () in
            
        }

    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        button.enabled = true
        button.titleLabel!.font =  UIFont.taskManagerMedium(22)
        userName.font = UIFont.taskManagerLight(20)
        password.font = UIFont.taskManagerLight(20)
        offlineUserButton.titleLabel!.font = UIFont.taskManagerLight(14)
        
        if let un = KeychainWrapper.stringForKey("username") {
            offlineUserButton.setTitle("Offline user: " + un, forState: .Normal)
        } else {
            offlineUserButton.setTitle("Offline user: none", forState: .Normal)
        }
        
        
        
        self.login(UIButton())
        
        var rightButtonBack:UIBarButtonItem = UIBarButtonItem(title: "Sign up", style: UIBarButtonItemStyle.Plain, target: self, action: "register:")
        
        
        self.navigationItem.title = "Hello there!"
        self.navigationItem.rightBarButtonItem = rightButtonBack
        
        
        password.secureTextEntry = true
    }
    override func viewWillAppear(animated: Bool) {
        login(UIButton())
    }
    
    override func viewWillDisappear(animated: Bool) {
        password.text = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        

    }
    
    
    @IBAction func loginButtonPressed(sender: AnyObject) {
        println("button pressed")
//        KeychainWrapper.stringForKey("password")
        
        if password.text == "" || userName.text == "" {
            let alert = UIAlertView()
            alert.title = "Empty field"
            alert.addButtonWithTitle("Ooops!")
            alert.show()
            
            return;
        }

        if button.titleLabel?.text == "Login" {
            
            NSLog("loging, user exist")
            println("cccc")
            
            let progressHUD = ProgressHUD(text: "Logging in")
            self.view.addSubview(progressHUD)
            
//            var differentUser:Bool = false
            
//            if name != userName.text {
//                var refreshAlert = UIAlertController(title: "Other User", message: "You are loggin as different user as home user is Do you want proceed?", preferredStyle: UIAlertControllerStyle.Alert)
//                
//                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
//                    differentUser = false
//                }))
//                
//                refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
//                    differentUser = true
//                }))
//                
//                presentViewController(refreshAlert, animated: true, completion: nil)
//                if differentUser {
//                    return;
//                }
//                
//            }
            
            
            model.authorizeUser(userName.text, password: password.text)  { (responseObject, error) -> () in
                if error != nil {
                    
                    println("nehehe")
                    
                    let pass:String? = KeychainWrapper.stringForKey("password")
                    
                    println(pass)
                    if pass == self.password.text {
                        println("okookokoko")
                        //offline mode
                        let alert = UIAlertView()
                        alert.title = "Offile mode"
                        alert.addButtonWithTitle("Ok")
                        progressHUD.hide()
                        alert.show()
                        
                        self.performSegueWithIdentifier("LoginSegue", sender: self)
                        return;
                    } else {
                        println("okookokoko1")
                        //offline mode
                        let alert = UIAlertView()
                        alert.title = "Bad Password"
                        alert.addButtonWithTitle("Ok")
                        progressHUD.hide()
                        alert.show()
                        
                        return;
                    }
                    
                    
                } else {
                    
                    NSLog("authenticating")
                    self.json = responseObject! as JSON
                    
                    if self.json["error"] != nil {
                        
                        let alert = UIAlertView()
                        alert.title = self.json["error"].string!
                        alert.addButtonWithTitle("Ooops!")
                        alert.show()
                        progressHUD.hide()
                        return;
                        
                    } else {
                        
                        println(self.json)
                        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                        appDelegate.currentToken = self.json["token"].string!
                        
                        self.performSegueWithIdentifier("LoginSegue", sender: self)
                        progressHUD.hide()
                    }
                }
            }
        } else {
            // register
            NSLog("registering, creaing new user on server")
            
            let progressHUD = ProgressHUD(text: "Registering")
            self.view.addSubview(progressHUD)
      
            model.registerUser(userName.text, password: password.text) { (responseObject, error) -> () in
                self.button.setTitle("Registering", forState: UIControlState.Normal)
                self.button.enabled = false
                if error != nil {
                    NSLog("register - error")
                    println(error)
//                    NSLog(error?.debugDescription)
                } else {
                    self.json = responseObject! as JSON
                    
                    if self.json["error"] != nil {
                        
                        let alert = UIAlertView()
                        alert.title = self.json["error"].string!
                        alert.addButtonWithTitle("Ooops!")
                        alert.show()
                        self.button.enabled = true
                        self.button.setTitle("Register", forState: .Normal)
                        progressHUD.hide()
                        return;
                    } else {
                        
                        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                        appDelegate.currentToken = self.json["token"].string!
                        
                        self.defaults.setValue(self.json["user"]["username"].string!, forKey: "username")
                        self.defaults.synchronize()
                        
                        KeychainWrapper.setString(self.userName.text, forKey: "username")
                        KeychainWrapper.setString(self.password.text, forKey: "password")
                        self.offlineUserButton.setTitle("Offline user: " + self.userName.text, forState: .Normal)
                        self.performSegueWithIdentifier("LoginSegue", sender: self)
                        progressHUD.hide()
                        
                        println(self.json)
                        println(self.json["user"]["username"])
                    }
                }
                self.button.enabled = true
                self.button.setTitle("Register", forState: .Normal)
                
            }
        }
        
    }
}
