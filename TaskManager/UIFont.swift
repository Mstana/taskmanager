//
//  UIFont.swift
//  
//
//  Created by Marek Stana on 14/10/15.
//
//

import UIKit

extension UIFont {
    
    class func taskManagerLight(size:CGFloat) -> UIFont {
        
        return UIFont(name: "Gotham-Light", size: size)!
    }
    
    class func taskManagerMedium(size:CGFloat) -> UIFont {
        
        return UIFont(name: "Gotham-Medium", size: size)!
    }
}