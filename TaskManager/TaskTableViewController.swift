//
//  TaskTableViewController.swift
//  
//
//  Created by Marek Stana on 12/10/15.
//
//

import UIKit
import SWTableViewCell
import CoreDataServerKit


class TaskTableViewController: UITableViewController, SWTableViewCellDelegate {
    
    var emptyLabel:UILabel!
    var tasks = []
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let model:DataModel = DataModel()
 
    override func viewDidLoad() {
        super.viewDidLoad()

        refresh()
        println(tasks)
        
        self.setNavigationBar()
        tableView.tableFooterView = UIView()
    }
    func setNavigationBar() {
        // NAVIGATION BAR SETTINGS
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add Task", style: UIBarButtonItemStyle.Plain, target: self, action: "addTask:")
        self.navigationItem.title = "My list"
        
        self.navigationItem.leftBarButtonItem  = UIBarButtonItem(title: "Sing Out", style: UIBarButtonItemStyle.Plain, target: self, action: "singOut:")
        
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.taskManagerTextGrey()
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.taskManagerTextGrey()
        self.navigationController?.navigationBar.titleTextAttributes = [
            
            NSFontAttributeName: UIFont.taskManagerMedium(22),
            NSForegroundColorAttributeName: UIColor.taskManagerLightBlue()
        ]
        
    }
    
    func singOut(sender:UIButton) {
        
        self.navigationController?.popToRootViewControllerAnimated(true)
    }

    func addTask(sender:UIButton) {
        
        var alert = UIAlertController(title: "Add task", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        alert.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
            textField.placeholder = "Task Title"
        })
        
        let progressHUD = ProgressHUD(text: "")
        self.view.addSubview(progressHUD)
        
        var action = UIAlertAction(title: "Add", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            
            let textField = alert.textFields![0] as! UITextField
            
            
            TaskManager.createTaskWithCD(self.appDelegate.currentToken, name: textField.text, completed: false){ (responseObject, error) -> () in
    
                self.refresh()
                progressHUD.hide()
            }
          }
        
        alert.addAction(action)
        
        self.presentViewController(alert, animated: true) { () -> () in

        }
    }


    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return tasks.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("taskTableViewCell", forIndexPath: indexPath) as! TaskTableViewCell

        var row = indexPath.row
        var currentTask:Task = tasks[row] as! Task
        
        cell.title.font = UIFont.taskManagerLight(18)
        cell.title.textColor = UIColor.taskManagerTextGrey()
        
        
            cell.title.text = currentTask.getName()
        
        cell.id = currentTask.getId()
        
        
        var color:UIColor = UIColor.taskManagerRedDelete()
        
            cell.completed = currentTask.getCompleted()
            
            if currentTask.getCompleted() {
                
                var utilityButtons: NSMutableArray = NSMutableArray()
                utilityButtons.sw_addUtilityButtonWithColor(UIColor.taskManagerGreen(), icon: UIImage(named: "check@2x"))

                cell.leftUtilityButtons = utilityButtons as NSArray as [AnyObject];
            } else {
                
                var utilityButtons: NSMutableArray = NSMutableArray()
                utilityButtons.sw_addUtilityButtonWithColor(UIColor.taskManagerRedDelete(), icon: UIImage(named: "cross@2x"))
                cell.leftUtilityButtons = utilityButtons as NSArray as [AnyObject];
            }
            
        
        
        var utilityButtons: NSMutableArray = NSMutableArray()
        utilityButtons.sw_addUtilityButtonWithColor(UIColor.taskManagerRedDelete(), icon: UIImage(named: "cross@2x"))
        cell.rightUtilityButtons = utilityButtons as NSArray as [AnyObject];
        
        cell.delegate = self;
        return cell
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
    }
    
    
    func swipeableTableViewCell(cell: SWTableViewCell!, didTriggerLeftUtilityButtonWithIndex index: Int) {
        

 
        
        
        let indexPath = self.tableView.indexPathForCell(cell)
        let row:Int! = indexPath?.row
        let taskCell = self.tableView.cellForRowAtIndexPath(indexPath!) as! TaskTableViewCell
        println(index)
        switch index {
            
        case 0:
            
            let progressHUD = ProgressHUD(text: "")
            self.view.addSubview(progressHUD)
            TaskManager.updateTaskWithCD(self.appDelegate.currentToken, name: tasks[row].getName(), completed: !tasks[row].getCompleted(), task: tasks[row] as! Task){ () -> () in
                
                self.refresh()
                progressHUD.hide()
            }

            self.refresh()
            progressHUD.hide()
            break;
        default:
            NSLog("Unsupported operation")
            break;
        }
        cell.hideUtilityButtonsAnimated(true)

    }
    
    func swipeableTableViewCell(cell: SWTableViewCell!, didTriggerRightUtilityButtonWithIndex index: Int) {
        
        let indexPath = self.tableView.indexPathForCell(cell)
        let row:Int! = indexPath!.row
        let taskCell = self.tableView.cellForRowAtIndexPath(indexPath!) as! TaskTableViewCell
        println(index)
        switch index {
            
        case 0:
            let progressHUD = ProgressHUD(text: "")
            self.view.addSubview(progressHUD)
            TaskManager.deleteTaskWithCD(self.appDelegate.currentToken, task: tasks[row] as! Task){ () -> () in
                
                self.refresh()
                progressHUD.hide()
            }

            break;
        default:
            NSLog("Unsupported operation")
            break;
        }
        cell.hideUtilityButtonsAnimated(true)
        
    }
    
    func swipeableTableViewCellShouldHideUtilityButtonsOnSwipe(cell: SWTableViewCell!) -> Bool {
        return false
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! TaskTableViewCell
        
        
        var alert = UIAlertController(title: "Update task", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        alert.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
            textField.placeholder = "Task Title"
            textField.text = cell.title.text
        })
        
        var action = UIAlertAction(title: "Update", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            
            
            let textField = alert.textFields![0] as! UITextField
     
            let progressHUD = ProgressHUD(text: "Updating")
            self.view.addSubview(progressHUD)
            TaskManager.updateTaskWithCD(self.appDelegate.currentToken,name: textField.text, completed: false, task: self.tasks[indexPath.row] as! Task){ () -> () in
                
                self.refresh()
                progressHUD.hide()
            }
        }
        
        alert.addAction(action)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    

    func refresh()
    {
        
        TaskManager.fetchTasks(appDelegate.currentToken) { (responseObject) -> () in
            
            self.tasks = responseObject
            self.tableView.reloadData()
        }
    }
}
