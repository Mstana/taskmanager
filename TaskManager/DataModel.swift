//
//  DataModel.swift
//  
//
//  Created by Marek Stana on 12/10/15.
//
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON



let URL = "http://api.taskstest.thefuntasty.com/"
let URL_REGISTER = URL + "register"
let URL_AUTHORIZE = URL + "authorize"


class DataModel {
    
    func registerUser(username:String, password:String, completionHandler: (responseObject: JSON?, error: NSError?) -> ()) {
        
        let mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: URL + "register")!)
        mutableURLRequest.HTTPMethod = "POST"
        mutableURLRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        mutableURLRequest.HTTPBody = ("{\n  \"username\": \"" + username + "\",\n  \"password\": \"" + password + "\"\n}").dataUsingEncoding(NSUTF8StringEncoding);
        
        var manager = Alamofire.Manager.sharedInstance
        var request = manager.request(mutableURLRequest)
        request.responseJSON{(request, response, data, error)  in
            if ((error) != nil) {
                completionHandler(responseObject: nil, error: error)
            } else {
                completionHandler(responseObject: JSON(data!) as JSON, error: error)
            }
            println(error)
            println(response)
        }
    }
    
    
    func authorizeUser(username:String, password:String, completionHandler: (responseObject: JSON?, error: NSError?) -> ()) {
        
        let mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: URL + "authorize")!)

        mutableURLRequest.HTTPMethod = "POST"
        mutableURLRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        mutableURLRequest.HTTPBody = ("{\n  \"username\": \"" + username + "\",\n  \"password\": \"" + password + "\"\n}").dataUsingEncoding(NSUTF8StringEncoding);
        
        var manager = Alamofire.Manager.sharedInstance
        var request = manager.request(mutableURLRequest)

        request.responseJSON{(request, response, data, error) in
            if ((error) != nil) {
                completionHandler(responseObject: nil, error: error)
            } else {
                completionHandler(responseObject: JSON(data!) as JSON, error: error)
            }

            println(error)
            println(response)
           
        }

    }


    func getAllTasks(token:String, completionHandler: (responseObject: JSON?, error: NSError?) -> ()) {
        
        var mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: URL + "me/tasks")!)
        mutableURLRequest.setValue("Bearer " + token, forHTTPHeaderField: "Authorization")
        
        
        var manager = Alamofire.Manager.sharedInstance
        var request = manager.request(mutableURLRequest)
        
                request.responseJSON{(request, response, data, error) in
                    if ((error) != nil) {
                        completionHandler(responseObject: nil, error: error)
                    } else {
                        completionHandler(responseObject: JSON(data!) as JSON, error: error)
                    }
                    println(error)
                    println(response)
                }
    }
    
    func createTask(token:String, title:String, completed:Bool, completionHandler: (responseObject: JSON?, error: NSError?) -> ()) {
        
        
        var mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: URL + "me/tasks")!)
        mutableURLRequest.setValue("Bearer " + token, forHTTPHeaderField: "Authorization")
        mutableURLRequest.setValue("application/json", forHTTPHeaderField:  "Content-Type")
        mutableURLRequest.HTTPMethod = "POST"
        mutableURLRequest.HTTPBody = ("{\n  \"name\": \"" + title + "\",\n  \"completed\": " + completed.description + "\n}").dataUsingEncoding(NSUTF8StringEncoding);
        
        var manager = Alamofire.Manager.sharedInstance
        var request = manager.request(mutableURLRequest)
        
        
        request.responseJSON{(request, response, data, error) in
            if ((error) != nil) {
                completionHandler(responseObject: nil, error: error)
            } else {
                completionHandler(responseObject: JSON(data!) as JSON, error: error)
            }
//            println(error)
//            println(response)
        }
    }
    
    func updateTask(token:String, id:String?, title:String, completed:Bool, completionHandler: (responseObject: JSON?, error: NSError?) -> ()) {
        
        
        var mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: URL + "me/tasks/" + id! )!)
        mutableURLRequest.setValue("Bearer " + token, forHTTPHeaderField: "Authorization")
        mutableURLRequest.setValue("application/json", forHTTPHeaderField:  "Content-Type")
        mutableURLRequest.HTTPMethod = "PUT"
        mutableURLRequest.HTTPBody = ("{\n  \"name\": \"" + title + "\",\n  \"completed\": " + completed.description + "\n}").dataUsingEncoding(NSUTF8StringEncoding);
        
        
//        let headers = [
//            "Authorization": "Bearer " + token,
//            "Content-Type": "application/json"
//        ]
//        let parameters = [
//            "name" : title,
//            "completed": completed.description
//        ]
//
//        manager.request(.PUT, URL + "me/tasks/" + id, headers: headers, parameters: parameters)
//            .responseJSON { response in
//                debugPrint(response)
//        }
        
        var manager = Alamofire.Manager.sharedInstance
        var request = manager.request(mutableURLRequest)
        
        
        request.responseJSON{(request, response, data, error) in
            if ((error) != nil) {
                completionHandler(responseObject: nil, error: error)
            } else {
                completionHandler(responseObject: JSON(data!) as JSON, error: error)
            }
            
            println(error)
            println(response)
        }
    }
    
    func deleteTask(token:String, id:String, completionHandler: (responseObject: JSON?, error: NSError?) -> ()) {
        
        
        var mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: URL + "me/tasks/" + id)!)
        mutableURLRequest.setValue("Bearer " + token, forHTTPHeaderField: "Authorization")
        mutableURLRequest.setValue("application/json", forHTTPHeaderField:  "Content-Type")
        mutableURLRequest.HTTPMethod = "DELETE"
        
        var manager = Alamofire.Manager.sharedInstance
        var request = manager.request(mutableURLRequest)
        
        
        request.responseJSON{(request, response, data, error) in
            if ((error) != nil) {
                completionHandler(responseObject: nil, error: error)
            } else {
                completionHandler(responseObject: JSON(data!) as JSON, error: error)
            }
            println(error)
            println(response)
        }
    }
    
}