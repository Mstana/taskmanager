//
//  Task.swift
//  
//
//  Created by Marek Stana on 15/10/15.
//
//

import Foundation
import CoreData


@objc(Task)
public class Task: NSManagedObject {

    @NSManaged var completed: NSNumber
    @NSManaged var name: String
    @NSManaged var taskId: String?
    @NSManaged var synced: String
    @NSManaged var userId: NSNumber
    
    
    public func getId() -> String? {
        
        return taskId
    }
    
    public func setId(id: String) {
        
        taskId = id
    }
    
    public func getName() -> String {
        return name
    }
    public func getCompleted() -> Bool {
        return completed as! Bool
    }
}
