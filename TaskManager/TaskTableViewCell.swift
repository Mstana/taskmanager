//
//  TaskTableViewCell.swift
//  
//
//  Created by Marek Stana on 13/10/15.
//
//

import UIKit
import SWTableViewCell

class TaskTableViewCell: SWTableViewCell {

    @IBOutlet weak var taskImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    var id: String!
    var completed:Bool!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
