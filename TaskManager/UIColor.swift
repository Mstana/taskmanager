//
//  UIColor.swift
//  
//
//  Created by Marek Stana on 14/10/15.
//
//

import UIKit

extension UIColor {
    
    class func taskManagerLightBlue() -> UIColor {

        return UIColor(red: 0.012, green: 0.643, blue: 0.824, alpha: 1.0)
    }
    
    class func taskManagerLightGreen() -> UIColor {
        
        return UIColor(red: 0.659, green: 0.878, blue: 0.431, alpha: 1.0)
    }
    
    class func taskManagerRedDelete() -> UIColor {
        
        return UIColor(red: 0.882, green: 0.373, blue: 0.361, alpha: 1.0)
    }
    
    class func taskManagerGreen() -> UIColor {
        
        return UIColor(red: 0.659, green: 0.878, blue: 0.431, alpha: 1.0)
    }
    
    class func taskManagerTextGrey() -> UIColor {
//        [NSColor colorWithCalibratedRed:0.702f green:0.702f blue:0.702f alpha:1.00f]
        return UIColor(red: 0.702, green: 0.702, blue: 0.702, alpha: 1.0)
    }

}
